import { useState } from 'react';
import PropTypes from 'prop-types';

function BlogPost({title, content}) {
  //state holding a boolean value to determine whether a post is expanded or not
  const [expanded, setExpanded] = useState(false);

  // //When the read more text is clicked, expanded value is set to the opposite value and text is changed
  const handleClick = () => {
    setExpanded(!expanded);
  }

  //render only title if expanded = false and content if expanded = true
  return (
    <div className="blog-post" style={{ borderBottom: 'thin solid gray', padding: '1em', }}>
      <span className="showMore">
        <h4>{title}</h4>
        <button onClick={handleClick}>{expanded ? 'Close' : 'Click to read more'}</button>
      </span>
      {
        expanded ?
          <div className="expanded">
            <p>{content}</p>
          </div> :
          <></>
        }
    </div>
  );
}

//validate prop types
BlogPost.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
}

export default BlogPost;
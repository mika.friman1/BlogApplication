import { useState } from "react";
import PropTypes from 'prop-types';

const initialState = { title: '', content: '' };
export default function AddBlogPost({ addPost }) {

  // set a state object to handle user input
  const [blogPost, setBlogPost] = useState(initialState);
  const [error, setError] = useState('');

  //states for expanding content and changing button text
  const [expanded, setExpanded] = useState(false);

  //handle click on the submit button
  const handleClick = () => {
      if (!blogPost.title || !blogPost.content) {
          setError('Invalid details, title and content are required')
          return;
      }
      setError('');
      addPost(blogPost);
      setBlogPost(initialState);
      setExpanded(false);
  }

  //When the "press here" button is pressed, expanded value is set to the opposite value and button text is changed
  const handleExpand = () => {
      setExpanded(!expanded);
      setError('');
  }

  return(
      <>
      <h2>Add a new blog post</h2>
      <span className="showMore" onClick={handleExpand}>
          <button>{expanded ? 'Close' : 'Add new'}</button>
      </span>
      {
          error?.length > 0 && <div style={{ backgroundColor: 'red', padding: '1em', color: 'white' }}>{error}</div>
      }
      {
        expanded ? (
          <div className="add-post">
          <label htmlFor="title">Enter title: <input id ="title" name="title" value={blogPost.title} onChange={(e) => setBlogPost({...blogPost, title: e.target.value})}/></label>

          <br />
          <label htmlFor="content">Enter content:</label>
          <textarea id="content" name="content" value={blogPost.content} onChange={(e) => setBlogPost({...blogPost, content: e.target.value})} ></textarea>
          <br />
          <button onClick={handleClick}>Submit</button>
          <br />
          </div>

        ) : <></> }

      </>
  );
}

//validate prop types
AddBlogPost.propTypes = {
    addPost: PropTypes.func.isRequired,
};
import BlogPost from "./BlogPost";
import PropTypes from 'prop-types';

export default function BlogList({ posts }) {

  //map blogPosts and push data to BlogPost component as props
  return (
    <>
    <div className="blog-list">
      <h2>See all posts</h2>
      {
        (posts || []).map((blogPost, index) => {
          return <BlogPost key={index} title={blogPost.title} content={blogPost.content} />
        })
      }
    </div>
    </>
  );
}

//validate prop types
BlogList.propTypes = {
  posts: PropTypes.array, // Adjust the PropTypes based on the actual type of the refresh prop
};